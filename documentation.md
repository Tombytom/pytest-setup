# Useful links

- Python packaging tutorial: https://packaging.python.org/en/latest/tutorials/packaging-projects/
- Pyprojec.toml infos: https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/
- Installing package with pip in editable mode: https://docs.pytest.org/en/7.1.x/explanation/goodpractices.html#goodpractices

```pip install -e .```

# Folder structure
```
pyproject.toml
README.md
src/
    mypkg/
        __init__.py
        app.py
        view.py
tests/
    test_app.py
    test_view.py
    ...
```
