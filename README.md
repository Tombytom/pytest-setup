# pytest Testing Project

This is a basic testing setup with pytest.

## Basic setup

### Change the project settings to match your python version

Check the `pyproject.toml` file to change the variable `requires-python` to match the version you intend to use.

### Create a virtual environment for yourself and install the dependencies

```sh
python -m venv venv-name
source venv-name/bin/activate
```

Install the dependencies, I recommend `pytest` and `mypy` to get you started.

I have written (venv-name) in front of every prompt to indicate that you must
have your virtual environment activated for these to work.

```sh
(venv-name) pip install pytest
(venv-name) pip install mypy
```

Check the `pyproject.toml` again, and compare the version numbers of `pytest`
and `mypy` with what you find there!

Finally, locally install your module by typing

```sh
pip install -e .
```
