#!/usr/bin/env python3

from mathfuncs.mathfuncs import NumOps

n = NumOps()

class TestMathFunctions:
    def test_always_passes(self):
        assert True

    def test_sum(self):
        assert n.sum(1,3) == 4
