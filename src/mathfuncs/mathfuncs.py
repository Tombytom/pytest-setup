#!/usr/bin/env python3
from typing import Optional

class NumOps:
    def sum (self, a: int, b: int) -> int:
        return a + b

if __name__ == "__main__":
    n = NumOps()
    print(n.sum(1,3))
